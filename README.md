# Quer ser desenvolvedor frontend na MDsystem Web?
Criamos este teste para avaliar seus conhecimentos e habilidades frontend (HTML, CSS e JavaScript).
Diferencial PHP...

## O teste
O desafio é tornar o layout abaixo em uma página funcional.  
Não existe certo ou errado, queremos ver como você se sai em situações reais, como este desafio.   
O tempo necessário para completar o desafio é de 4 dias após o envio do e-mail.

Portanto, dê o seu melhor!

:warning: **OBS:** Os layouts podem ser encontrados na pasta **layout**

![Layout](layout/desktop.png)

## Instruções
- O conteúdo não é estático. Você deve criar um JavaScript para consultar a informações de sobre nós e serviços.  
Os dados serão fornecidos por uma API. As instruções estão mais abaixo.
- Fonte padrão: "Poppins"
- Você pode utilizar as tecnologias (frameworks ou bibliotecas JS)
- Crie uma documentação simples comentando sobre as tecnologias e soluções adotadas
- Se necessário explique também como rodar o seu projeto

## Requisitos
- Design responsivo nos breakpoints 320px, 768px, 1024px e 1440px
- Suporte para IE, Chrome, Safari, Firefox

## Diferenciais
- Uso de pré-processadores CSS (Sass, Less)
- Utilizar alguma automatização (Grunt, Gulp, ...)

## O que será avaliado
- Estrutura e organização do código e dos arquivos
- Soluções adotadas
- Tecnologias utilizadas
- Qualidade
- Fidelidade ao layout
- Enfim, tudo será observado e levado em conta

## Como iniciar o desenvolvimento
- Instale o [npm](https://nodejs.org/en/download/)
- Fork este repositório na sua conta do Bitbucket
- Crie uma branch com o nome **desafio**
- Instale as dependências
```
npm install
```
- Rode a aplicação
```
npm start
```
- Acesse http://localhost:8888
- Realize o desenvolvimento na pasta public

## Como enviar seu teste
- Envie um email para [diogo.alves@mdsystemweb.com.br] com o link do seu repositório
- Se o seu repositório for privado, solicite os emails das pessoas responsáveis para conceder acesso de leitura ao seu repositório.
